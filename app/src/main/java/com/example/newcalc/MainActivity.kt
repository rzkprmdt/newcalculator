package com.example.newcalc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var canAddOperation = false
    private  var canAddDecimal = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun backSpace(view: View) {
        val length = workingPanel.length()
        if (length>0)
            workingPanel.text = workingPanel.text.subSequence(0,length -1)
    }
    fun equalsAction(view: View) {
        callResultActivity()
    }

    private fun callResultActivity() {
        val result = calculateResult()
        val intent = Intent(this, MainActivity2::class.java).also{
            it.putExtra("MESSAGE", result)
            startActivity(it)
        }
    }

    private fun calculateResult(): String {

        val digitsOperator = digitsOperator()
        if (digitsOperator.isEmpty())
            return ""
        val timesDivision = timesDivisionCalculate(digitsOperator)

        if (timesDivision.isEmpty())
            return ""
        val result = addSubstractCalculate(timesDivision)
        return result.toString()
    }

    private fun addSubstractCalculate(passedList: MutableList<Any>): Any {
        var result = passedList[0] as Float
        for(i in passedList.indices)
        {
            if(passedList[i] is Char && i != passedList.lastIndex)
            {
                val operator = passedList[i]
                val nextDigit = passedList[i + 1] as Float
                if (operator == '+')
                    result += nextDigit
                if (operator == '-')
                    result -= nextDigit
            }
        }

        return result
    }


    private fun timesDivisionCalculate(passedList: MutableList<Any>): MutableList<Any> {
        var list = passedList
        while (list.contains('x') || list.contains('/')){
            list = calcTimesDiv(list)
        }
        return list
    }

    private fun calcTimesDiv(passedList: MutableList<Any>): MutableList<Any> {
        val newList = mutableListOf<Any>()
        var restartIndex = passedList.size
        for (i in passedList.indices){
            if (passedList[i] is Char && i != passedList.lastIndex && i<restartIndex){
                val operator = passedList[i]
                val prevDigit = passedList[i-1] as Float
                val nextDigit = passedList[i+1] as Float
                when (operator){
                    'X' -> {
                        newList.add(prevDigit * nextDigit)
                        restartIndex = i + 1
                    }
                    '/' -> {
                        newList.add(prevDigit / nextDigit)
                        restartIndex = i + 1
                    }
                    else -> {
                        newList.add(prevDigit)
                        newList.add(operator)
                    }
                }
            }
            if (i>restartIndex){
                newList.add(passedList[i])
            }
        }
        return newList
    }


    private fun digitsOperator() : MutableList<Any>{
        val list = mutableListOf<Any>()
        var currentDigit = ""
        for (character in workingPanel.text){
            if (character.isDigit() || character.equals(".")){
                currentDigit += character
            }
            else{
                list.add(currentDigit.toFloat())
                currentDigit = ""
                list.add(character)
            }
        }
        if (currentDigit != ""){
            list.add(currentDigit.toFloat())
        }
        return list
    }

    fun allClear(view: View) {
        workingPanel.text =""
        resultPanel.text = ""

    }
    fun numberAction(view: View) {
        if (view is Button) {
            if (view.text == "."){
                if (canAddDecimal)
                    workingPanel.append(view.text)
                canAddDecimal = false
            }
            else
                workingPanel.append(view.text)
            canAddOperation = true
        }
    }

    fun operateAction(view: View) {
        if (view is Button && canAddOperation) {
            workingPanel.append(view.text)
            canAddOperation = false
            canAddDecimal = true
        }
    }

    fun decimalAction(view: View) {
        if (view is Button && canAddDecimal) {
            workingPanel.append(view.text)
            canAddDecimal = false
        }
    }
}